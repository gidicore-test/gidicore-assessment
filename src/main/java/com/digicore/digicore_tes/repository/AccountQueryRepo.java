package com.digicore.digicore_tes.repository;

import com.digicore.digicore_tes.model.entity.AccountInfo;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountQueryRepo {

    AccountInfo findAccountInfoByAccountName(String accountNumber);
//    List<Stocks> findAll();
}
