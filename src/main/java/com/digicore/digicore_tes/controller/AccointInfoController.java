package com.digicore.digicore_tes.controller;

import com.digicore.digicore_tes.model.request.AccountDepositRequest;
import com.digicore.digicore_tes.model.request.AccountInfoRequest;
import com.digicore.digicore_tes.model.request.AccountWithdrawalRequest;
import com.digicore.digicore_tes.model.request.CreateAccountRequest;
import com.digicore.digicore_tes.model.response.AccountInfoResponse;
import com.digicore.digicore_tes.model.response.AccountOperationResponse;
import com.digicore.digicore_tes.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/accounts")
public class AccointInfoController {

    @Autowired
    public AccountService accountService;

    @PostMapping
    @RequestMapping(
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity createAccount(
             @RequestBody CreateAccountRequest accountInfoRequest) throws Exception{
        ResponseEntity successResponse = accountService.createAccount(accountInfoRequest);
        return  successResponse;
    }

    @RequestMapping(method = RequestMethod.POST, path = "account_info",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity getAccountInformation(
           @RequestBody AccountInfoRequest accountInfoRequest) throws Exception{
        ResponseEntity successResponse = accountService.getAccountInformation(accountInfoRequest);
        return  successResponse;
    }

    @RequestMapping(method = RequestMethod.POST, path = "account_statement",
            consumes = "application/json",
            produces = "application/json")
    public AccountInfoResponse getAccountStatement(
           @RequestBody AccountInfoRequest accountInfoRequest) throws Exception{
        AccountInfoResponse successResponse = accountService.getAccountStatement(accountInfoRequest);
        return  successResponse;
    }

    @RequestMapping(method = RequestMethod.POST, path = "deposit",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity makeDeposit(
           @RequestBody AccountDepositRequest accountInfoRequest) throws Exception{
        ResponseEntity successResponse = accountService.depositToAccount(accountInfoRequest);
        return  successResponse;
    }

    @RequestMapping(method = RequestMethod.POST, path = "withdwral",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity makewithdrwal(
            @RequestBody AccountWithdrawalRequest accountInfoRequest) throws Exception{
        ResponseEntity successResponse = accountService.withdrwalFromAccount(accountInfoRequest);
        return  successResponse;
    }

}
